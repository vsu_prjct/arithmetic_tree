#include <iostream>
#include <string>
#include <fstream>

using namespace std;

struct Node{
	char ch;// �����. �������
	double v;// �����
	Node *r, *l; // ��������� �� ����� ������
};

/* ����� ����� */
void show_left(Node *&Tree){
	if (Tree != NULL){
		show_left(Tree->l);
		if ((Tree->ch == '+') || (Tree->ch == '-') || (Tree->ch == '*') || (Tree->ch == '/')) cout << Tree->ch;
		else cout << Tree->v;
		show_left(Tree->r);
	}
}

/* ����� ������������� ������ '(' */
int find_bkt(string S, int i){
	int k = 1;
	do{
		i--;
		switch (S[i]){
		case ')': k++; break;
		case '(': k--; break;
		}
	} while (i >= 0 && k > 0);
	if (i < 0){
		cout << "������! �� ������� ( ������";
		exit(0);
	}
	return i;
}
/* ����� �����. �������� ��� ����� */
int find_sign(string S){
	int j = 0, l = S.length() - 1;
	bool first = true;
	while (l >= 0 && S[l] != '+' && S[l] != '-'){
		switch (S[l]){
			case ')': l = find_bkt(S, l); break; // ������� ����� � �������
			case '*':
			case '/':
				if (first){
					j = l;
					first = false;
				}
			break;
			case '.':
			case ',':
				if (first) j = l;
			break;
		}
		l--;
	}
	if (l >= 0) return l;
	else return j;
}

/* ���������� ������, ������������ �������������� ��������� */
void Build(string S, Node *&Tree){
	int l = S.length() - 1;
	if (l == 0){
		Node *val = new Node;
		val->l = val->r = NULL;
		val->ch = '#';
		val->v = S[0] - '0';
		Tree = val;
	}
	else{
		// True ������ ����� ���-� ������� � ()
		if (S[l] == ')' && find_bkt(S, l) == 0){
			Build(S.substr(1, l - 1), Tree);// ��������� ��������� � ()
		}
		else{
			if (S[0] == '-' || S[0] == '+' || S[0] == '*' || S[0] == '/' || S[0] == '.' || S[0] == ','){
				Build('0' + S, Tree);// ���� -�����, �� 0-�����
			}
			else{
				int i = find_sign(S);
				Node *q = new Node;
				q->l = q->r = NULL;
				q->ch = (S[i] == ',')?'.':S[i];

				Build(S.substr(0, i), q->l);//�� �����
				Build(S.substr(i + 1, l - 1), q->r);//����� �����
				Tree = q;
			}
		}
	}
}

double Calc(Node *&Tree){
	if (Tree == NULL) return 0;
	switch (Tree->ch){
		case '#':  return Tree->v; break;
		case '.':  return Calc(Tree->l) + Calc(Tree->r)/10; break;
		case '+':  return Calc(Tree->l) + Calc(Tree->r); break;
		case '-':  return Calc(Tree->l) - Calc(Tree->r); break;
		case '*':  return Calc(Tree->l) * Calc(Tree->r); break;
		case '/':  return Calc(Tree->l) / Calc(Tree->r); break;
		default: cout << "������"; return -1; break;
	}
}

int main(){
	setlocale(0, "");

	Node *tree = NULL;

	string str;
	ifstream f("file.txt");
	while (getline(f, str)){
		cout << str;
		Build(str, tree);
		cout << " [TREE ";
		show_left(tree);
		cout << " ]";
		cout << " = " << Calc(tree) << endl;
	}
	f.close(); // ��������� ����

	cin.get();
	cin.get();
	return 0;
}